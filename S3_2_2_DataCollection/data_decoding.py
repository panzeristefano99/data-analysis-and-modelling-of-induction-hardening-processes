"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.9
Description: Decoder for UDP message coming from PLC
"""

import struct
import pandas as pd
import os
import logging
from datetime import datetime


class Packet:

    def __init__(self):

        # Log creation
        self.logger = logging.getLogger("File_logger")
        self.logger.setLevel(logging.DEBUG)
        fl = logging.FileHandler(os.path.expanduser('~') + '/Documents/software_log.log')
        fl.setLevel(logging.DEBUG)
        self.logger.addHandler(fl)

        # CSV Template - Setup
        self.path_template_csv = os.path.expanduser('~') + '/Documents/DataCollection_PythonSW/DataPacket_PLC.csv'
        data_frame = pd.read_csv(self.path_template_csv, sep=";")
        self.variables_names = data_frame.Name
        self.variables_dataTypes = data_frame.Type
        self.size = data_frame.Size

        # Paths for saving csv
        self.base_path = os.path.expanduser('~') + '/Documents/DataFiles/'
        self.actual_month = datetime.now().strftime('%B')
        self.actual_day = datetime.now().strftime('%d')
        self.file_name = "Initialized_name.csv"

        # Size of UDP packet
        self.LENGTH = self.size.sum()  # Sum size elements of csv

        # Byte sizes for every variable type (read size from CSV if exists, if not, assign default value)
        self.BOOL_SIZE = data_frame.loc[data_frame['Type'] == 'Bool', 'Size'].iloc[0] \
            if 'Bool' in data_frame['Type'].values else 1
        self.BYTE_SIZE = data_frame.loc[data_frame['Type'] == 'Byte', 'Size'].iloc[0] \
            if 'Byte' in data_frame['Type'].values else 1
        self.INT_SIZE = data_frame.loc[data_frame['Type'] == 'Int', 'Size'].iloc[0] \
            if 'Int' in data_frame['Type'].values else 2
        self.REAL_SIZE = data_frame.loc[data_frame['Type'] == 'Real', 'Size'].iloc[0] \
            if 'Real' in data_frame['Type'].values else 4
        self.STRING_SIZE = data_frame.loc[data_frame['Type'] == 'String[30]', 'Size'].iloc[0] \
            if 'String[30]' in data_frame['Type'].values else 32
        self.FC_ERROR_SIZE = data_frame.loc[data_frame['Type'] == 'FC_error', 'Size'].iloc[0] \
            if 'FC_error' in data_frame['Type'].values else 4
        self.PLC_ERROR_SIZE = data_frame.loc[data_frame['Type'] == 'PLC_error', 'Size'].iloc[0] \
            if 'PLC_error' in data_frame['Type'].values else 2

        # Format of elements for unpack
        self.BOOL_FORMAT = "?"  # Bool
        self.BYTE_FORMAT = "c"  # Byte of length 1
        self.INT_FORMAT = ">h"  # Short - (Big-endian)
        self.REAL_FORMAT = ">f"  # Float (Big-endian)
        self.STRING_FORMAT = "30s"  # 30 char
        self.FC_ERROR_FORMAT = ">I"  # Unsigned int (Big-endian)
        self.PLC_ERROR_FORMAT = ">H"  # Unsigned Short (Big-endian)

        # Counter variable to store previous variable in counter
        self.counter_previous = 0

    def data_decoding(self, data_UDP):
        """
        Method to decode and store the received data from the PLC by UDP protocol
        1. Decode the data according to the structure present in the csv template
        2. Stores the data in a Dictionary
        3. Saves the dictionary in a file (creating a new file for every new piece)
        :param data_UDP: data sent by the PLC
        :return: NO RETURN
        """
        position = 0  # Initial position to iterate the data packet
        timestamp_file = datetime.now().strftime("%Y-%m-%d--%H-%M-%S-%f")[:-3]  # Date/Time here for more accuracy
        if len(data_UDP) == self.LENGTH:  # Check packet length
            collected_data = {}  # Initial empty dictionary
            for j in range(len(self.variables_names)):  # Iterate the data according to the variables in the template

                # Assign length and format for each type (special storing format)
                if self.variables_dataTypes[j] == "String[30]":
                    length = self.STRING_SIZE
                    decode_format = self.STRING_FORMAT

                    # Store in dictionary and remove empty/0 bytes
                    collected_data[self.variables_names[j]] = \
                        struct.unpack(decode_format, data_UDP[position + 2:(position + length)])[0].decode("utf-8")
                    collected_data[self.variables_names[j]] = collected_data[self.variables_names[j]].replace(
                        "\x00", "")

                elif self.variables_dataTypes[j] == "Real":
                    length = self.REAL_SIZE
                    decode_format = self.REAL_FORMAT
                    # Round to four decimals for float numbers
                    collected_data[self.variables_names[j]] = \
                        round(struct.unpack(decode_format, data_UDP[position:(position + length)])[0], 4)

                else:
                    # Assign length and format for each type (standard storing format)
                    if self.variables_dataTypes[j] == "Bool":
                        length = self.BOOL_SIZE
                        decode_format = self.BOOL_FORMAT
                    if self.variables_dataTypes[j] == "Byte":
                        length = self.BYTE_SIZE
                        decode_format = self.BYTE_FORMAT
                    elif self.variables_dataTypes[j] == "Int":
                        length = self.INT_SIZE
                        decode_format = self.INT_FORMAT
                    elif self.variables_dataTypes[j] == "FC_error":
                        length = self.FC_ERROR_SIZE
                        decode_format = self.FC_ERROR_FORMAT
                    elif self.variables_dataTypes[j] == "PLC_error":
                        length = self.PLC_ERROR_SIZE
                        decode_format = self.PLC_ERROR_FORMAT

                    if self.variables_names[j] != 'DummyByte':  # Do not save in dictionary dummy byte
                        # Store in dictionary
                        collected_data[self.variables_names[j]] = \
                            struct.unpack(decode_format, data_UDP[position:(position + length)])[0]

                position = position + length  # Update next iteration position

            if collected_data['NC_Active']:  # Check if a program/job was active
                # Replace spaces and .nc in job-name (for correct file name)
                job_name = str(collected_data["JobName"]).strip().replace(" ", "-")
                job_name = job_name.replace(".nc", "")
                job_name = job_name.replace(".NC", "")
                if job_name == "":
                    job_name = "ActiveEmptyJob"
            else:
                job_name = "NoActiveJob"
                collected_data["JobName"] = ""

            collected_data["JobName"] = job_name  # Update Job name according to if it is active or not

            # Update actual month and day and create (if not existent) storage folder
            self.actual_month = datetime.now().strftime('%B') + '/'
            self.actual_day = self.actual_day = datetime.now().strftime('%d') + '/'
            os.makedirs(self.base_path + self.actual_month + self.actual_day, exist_ok=True)

            if collected_data['NewPiece']:  # If a new piece is collected, then create a new file
                self.file_name = timestamp_file + '_' + job_name + ".csv"
                data_frame = pd.DataFrame(collected_data, index=["new_sample"])
                data_frame.to_csv(self.base_path + self.actual_month + self.actual_day + self.file_name,
                                  mode='w', index=False, sep=";")

            else:  # If not, append to the last file
                mode = 'a'
                header = False
                if not os.path.exists(self.base_path + self.actual_month + self.actual_day + self.file_name) \
                        or self.counter_previous >= collected_data['Counter']:
                    # Error, not possible. It should exist already (previously created
                    # for new piece) or we lost the first package of New Piece
                    self.file_name = timestamp_file + '_' + job_name + "_ERROR_StartsWith_NotNewPiece.csv"
                    mode = 'w'
                    header = True
                data_frame = pd.DataFrame(collected_data, index=["new_sample"])
                data_frame.to_csv(self.base_path + self.actual_month + self.actual_day + self.file_name,
                                  mode=mode, header=header, index=False, sep=";")

            # Update last counter value
            self.counter_previous = collected_data['Counter']

        else:
            # Log length packet error
            raspi_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f")[:-3]
            self.logger.error("{} - Wrong packet length:\n"
                              "Length: {}\n"
                              "Contents: {}\n".format(raspi_time, len(data_UDP), data_UDP))
