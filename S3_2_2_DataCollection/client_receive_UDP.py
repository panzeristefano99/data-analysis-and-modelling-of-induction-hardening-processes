"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.9
Description: UDP client to receive new messages on socket from PLC
"""

import socket
from datetime import datetime
from data_decoding import Packet

UDP_IP = "192.168.1.201"  # PLC sending IP
UDP_PORT = 2003  # PLC sending port
sock = socket.socket(socket.AF_INET,  # Internet IPv4Int
                     socket.SOCK_DGRAM)  # UDP
# Create binding with the sending device by IP and Port
sock.bind((UDP_IP, UDP_PORT))

packet = Packet()  # Create packet class
raspi_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S.%f")[:-3]
packet.logger.info("{} - Program Started\n".format(raspi_time))

while True:
    # Wait for data on the selected UDP port
    data_UDP, addr = sock.recvfrom(4096)  # buffer size is 4096 bytes
    # Decode data
    packet.data_decoding(data_UDP)
