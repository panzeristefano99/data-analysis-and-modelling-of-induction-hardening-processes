Copyright (c) 2023, ENRX AS

- The data contained in this folder is property of ENRX AS, and is protected by copyright.
- The use of this data is ONLY allowed in order to execute the Python Scripts present in the actual repository that need it as an input.
- NO other use of the data is allowed, such as modification, distribution or appropriation of it.
