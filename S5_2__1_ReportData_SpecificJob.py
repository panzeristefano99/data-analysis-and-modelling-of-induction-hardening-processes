"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: ydata-profiling report over specific Client and job (filtered data)
"""

from ydata_profiling import ProfileReport
import pickle
import os

# Client, job and mode to analyse
client = 'Client2'
job = 'Job4'
mode = 'ALL'

# Folder with Filtered Data from Selected Client and Mode in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered', client, mode)

# Creating directories to save the Report
saving_path = os.path.join(os.getcwd(), 'LSTM_Files/DataPreparation')
os.makedirs(os.path.join(saving_path), exist_ok=True)

# Dataset info for the generated report
dataset_info = {
        "author": "Stefano Fernando Panzeri Reyes",
        "copyright_holder": "ENRX AS",
        "copyright_year": 2023
    }

# Import of general Dataframe for that specific job (within the selected mode) (pickle format)
df = pickle.load(open(os.path.join(pickle_folder, 'df_' + client + '_' + job + '.pickle'), 'rb'))
profile = ProfileReport(df, dataset=dataset_info, title="Exploratory Analysis: " + client + '-' + job,
                        duplicates=None) # Report generation with no calculation of duplicates
profile.to_file(os.path.join(saving_path, "Report_" + client + '_' + job + ".html"))  # Save of Report
