"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: Analysis of Heating Cycles of Clients and Histogram generation
"""

import pickle
import os
import matplotlib.pyplot as plt

# Folder with Client Data filtered in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')

saving_path = os.path.join(os.getcwd(), 'LSTM_Files/DataPreparation/HeatingCycleTimes')
os.makedirs(os.path.join(saving_path), exist_ok=True)

clients = ['Client1','Client2', 'Client3']  # Client names

# Create subdirectories for each one of the clients
for client in clients:
    os.makedirs(os.path.join(saving_path, client), exist_ok=True)

# Number of different jobs for each client
jobs_Client1 = 20
jobs_Client2 = 33
jobs_Client3 = 48

# Select the variables to use, in order to filtrate the df
variablesToUse = ["HeatingCycleTime", "Converter_Error"]


# Loop for data import and creation of Histograms for each client (over the general dictionary)
for client in clients:
    # Import Dictionary containing individual workpieces per client
    dictionary = pickle.load(open(os.path.join(pickle_folder, client, 'dictionary_df_' + client + '.pickle'), 'rb'))
    # Iterate over the different client jobs
    for job_number in range(1, globals()['jobs_' + client] + 1):
        heating_times = []  # Storage of Heating Cycle times per job
        job = 'Job' + str(job_number)
        for key in dictionary:  # Iterate the dictionary key by key (file by file)
            if key[25:] == job:  # Focus on the job under analysis in this loop
                df = dictionary[key]  # Import the df
                converterError = (df['Converter_Error'])  # Get the list of the converter Errors
                errors = list(converterError[converterError != 0])  # See if the file has some error different from 0
                if len(errors) == 0:  # Only analyse pieces in normal conditions (no errors)
                    HeatingCycleTime = df['HeatingCycleTime'].max()  # Get total Heating Cycle of that piece
                    if HeatingCycleTime > 0:  # Only analyse pieces where the heating has been turned ON
                        heating_times.append(HeatingCycleTime)
                    else:
                        continue

        # HISTOGRAM CREATION
        if len(heating_times) > 0: # Check if the job was used for heating in some of the cases
            fig, ax = plt.subplots(figsize=(10, 7))  # Create figure with corresponding size
            # Create Histogram bins every 20 s.
            bins = range(int(min(heating_times)), int(max(heating_times)) + 1, 20)
            if len(bins) < 2:  # If difference between min and max is less than 20, use min and max as bin size
                bins = [int(min(heating_times)), int(max(heating_times)) + 1]
            ax.hist(heating_times, bins=bins)  # Create histogram over obtain values with the calculated bins
            plt.xticks(bins)  # Use bins for X values
            plt.title('Heating Cycle Times_' + client + '-' + job)  # Add title
            # Add Axis labels
            plt.xlabel('Time (s)')
            plt.ylabel('Workpieces')
            plt.savefig(os.path.join(saving_path, client, 'Heating Cycle Times_' + client + '-' + job + '.png'))  # Save to image
            plt.close()
