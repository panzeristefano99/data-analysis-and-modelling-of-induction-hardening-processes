"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: Generation of Tables containing the type of Converter Error present in one client
"""

import os.path
import matplotlib.pyplot as plt
import pandas as pd
import pickle
from S5_1__2_ErrorWarningDecoding import error_decoding  # Error/Warning decoding function

# Client and job to analyse
client = 'Client2'
job = 'Job4'

# Folder with Client Data filtered in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')

# Creating directories to save the Tables
saving_path = os.path.join(os.getcwd(), 'LSTM_Files/DataPreparation')
os.makedirs(os.path.join(saving_path), exist_ok=True)

# Import Dictionary containing individual workpieces per client
dictionary = pickle.load(open(os.path.join(pickle_folder, client, 'dictionary_df_' + client + '.pickle'), 'rb'))

# Create a joblist with all the jobs present in the client
errorNames = []  # List containing the error names to set in the final table
errorCounter = pd.DataFrame(columns=['Error Name', 'Amount']) # Crate headers for counting table
# Iterate the dictionary key by key (file by file)
for key in dictionary:
    if key[25:] == job:  # Only focus on the selected job
        converterError = (dictionary[key]['Converter_Error'])  # Extract the list of all converter errors in the file
        errors = list(converterError[converterError != 0])  # Get only those different from 0
        unique_errors = list(set(errors))  # Filter to remove duplicated
        """
        - Let's assume an error message of 2 bits where the first bit indicates High Current 
          and the second oen indicates High Voltage.
        - From the DF it is possible to obtain in the same file several samples of 01 and several of 11 (afterwards).
        - "errors" till contain: [01,01,01,01,01,11,11,11,11,11]
        - When filtering that list, "unique_errors" will only contain: [01,11] 
        """
        listErrorNames = []  # List to store decoded error values
        for i in range(len(unique_errors)):  # Iterate through the different error values and decode them
            listErrorNames.extend(error_decoding(unique_errors[i]))
        listErrorNames_unique = list(set(listErrorNames))  # Filter to remove duplicates
        """
        - When decoding those, 01 will output: High Current; and 11 will output: [High Current, High Voltage]. This is 
          because in the second error value, the first is still contained.
        - The values present in "listErrorNames" are: [High Current, High Current, High Voltage]
        - Therefore, this list needs to be filtered again to obtain in "listErrorNames_unique": [High Current, High Voltage]
        """
        # Generation of Final Table Values
        for errorName in listErrorNames_unique: # The decoded names are iterated
            if errorName not in errorNames:   # If that name has not appeared before in another workpiece
                errorNames.append(errorName) # Add to general list (for table)
                errorCounter.loc[len(errorCounter.index)] = [errorName, 1] #  Start counter for that error to 1
            else:  # If already appeared before
                # Increment counter in 1 for that error name
                errorCounter.loc[errorCounter['Error Name'] == errorName, ['Amount']] = \
                    errorCounter.loc[errorCounter['Error Name'] == errorName, ['Amount']] + 1

# FIGURE CREATION
fig = plt.figure(figsize=(7, 3))  # Create figure with corresponding size
ax = fig.add_subplot(1, 1, 1)  # Create subplot within figure for table insertion
ax.axis('off')   # Turn off Axis
ax.table(cellText=errorCounter.values, colLabels=errorCounter.columns, loc='center', cellLoc='center')  # Add table
ax.set_title('Error Types ' + client + '-' + job)  # Add title
plt.savefig(os.path.join(saving_path, 'Error Types ' + client + '-' + job + '.png'), dpi=400)  # Save to image
