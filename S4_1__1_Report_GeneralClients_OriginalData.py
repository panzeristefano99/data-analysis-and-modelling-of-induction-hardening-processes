"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.9
Description: ydata-profiling report over Client data (not filtered)
"""

from ydata_profiling import ProfileReport
import pickle
import os

# Folder with Client Data in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData')

# Creating directories to save Report data
report_folder = os.path.join(os.getcwd(), 'EDA_Files/ReportData_GeneralClients_OriginalData')
os.makedirs(os.path.join(report_folder), exist_ok=True)

clients = ['Client1', 'Client2', 'Client3']  # Client names

# Dataset info for the generated report
dataset_info = {
        "author": "Stefano Fernando Panzeri Reyes",
        "copyright_holder": "ENRX AS",
        "copyright_year": 2023
    }

# Loop for data import and creation of Data Report for each general client Dataframe
for client in clients:
    for path, subdirs, files in os.walk(os.path.join(pickle_folder, client)):  # Analysing every client folder
        for name in files:
            if name.endswith('.pickle') and not name.startswith('dictionary'):
                df = pickle.load(open(os.path.join(path, name), 'rb'))  # Import of general Dataframe (pickle format)
                profile = ProfileReport(df, dataset=dataset_info, title="Exploratory Analysis: " + name[3:-7],
                                        duplicates=None)  # Report generation with no calculation of duplicates
                profile.to_file(os.path.join(report_folder, "Report_" + name[3:-7] + ".html"))  # Save of Report
