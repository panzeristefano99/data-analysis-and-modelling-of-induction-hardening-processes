"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: Creation of Pandas DF from CSV files and saved as Pickle
"""

import os
import pandas as pd
import pickle

# Path with all the CSV files from the three clients from April to July
DATA_PATH = os.path.join(os.getcwd(), 'DATA/ClientData')

# Creating general directory to save the converted data
pickle_data_folder = os.path.join(os.getcwd(), 'DATA/PickleData')
os.makedirs(os.path.join(pickle_data_folder), exist_ok=True)

clients = ['Client1', 'Client2', 'Client3']  # Client names

# Create subdirectories for each one of the clients
for client in clients:
    os.makedirs(os.path.join(pickle_data_folder, client), exist_ok=True)

# Creating relevant variables to store information
df_Client1 = pd.DataFrame()  # General dataframe with all the dataset of the client
dictionary_df_Client1 = {}  # Dictionary containing all the single dataframes (each csv/workpiece) of the client

df_Client2 = pd.DataFrame()
dictionary_df_Client2 = {}

df_Client3 = pd.DataFrame()
dictionary_df_Client3 = {}

# Loop for data import and creation of Pandas Dataframe for each imported file
for client in clients:
    for path, subdirs, files in os.walk(os.path.join(DATA_PATH, client)): # Analysing every client folder
        for name in files:
            if name.endswith('.csv') and 'ERROR' not in name:  # Check that is a .csv and does not contain any error
                df_read = pd.read_csv((os.path.join(path, name)))  # Import csv into a Pandas DF
                globals()['dictionary_df_' + client][name[:-4]] = df_read  # Save single csv in dictionary entry
                globals()['df_' + client] = pd.concat([globals()['df_' + client], df_read]) # Append csv to general DF

    # Reset index of the dataframe
    globals()['df_' + client] = globals()['df_' + client].reset_index(drop=True)
    # Assure that the datatypes which are not numbers are correctly detected (for future reporting)
    globals()['df_' + client]['JobName'] = globals()['df_' + client]['JobName'].astype('category')
    globals()['df_' + client]['C_AxisMode'] = globals()['df_' + client]['C_AxisMode'].astype('bool')
    globals()['df_' + client]['HeatingOn'] = globals()['df_' + client]['HeatingOn'].astype('bool')
    globals()['df_' + client]['EnergyAlarm_Low'] = globals()['df_' + client]['EnergyAlarm_Low'].astype('bool')
    globals()['df_' + client]['EnergyAlarm_High'] = globals()['df_' + client]['EnergyAlarm_High'].astype('bool')

    # Save the dataframe in the corresponding client subdirectory
    with open(os.path.join(pickle_data_folder, client, 'df_' + client + '.pickle'), 'wb') as f:
        pickle.dump(globals()['df_' + client], f)

    # Save the dictionary in the corresponding client subdirectory
    with open(os.path.join(pickle_data_folder, client, 'dictionary_df_' + client + '.pickle'), 'wb') as f:
        pickle.dump(globals()['dictionary_df_' + client], f)
