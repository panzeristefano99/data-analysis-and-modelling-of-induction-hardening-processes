"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: Generation of Tables containing the number of error-types per Client and per Job
"""

import os.path
import matplotlib.pyplot as plt
import pandas as pd
import pickle

# Folder with Client Data filtered in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')

# Creating directories to save the Tables
saving_path = os.path.join(os.getcwd(), 'LSTM_Files/DataPreparation')
os.makedirs(os.path.join(saving_path), exist_ok=True)

clients = ['Client1', 'Client2', 'Client3']  # Client names

# Loop for data import and creation of Error Types Table for each client (over the general dictionary)
for client in clients:
    # Import Dictionary containing individual workpieces per client
    dictionary = pickle.load(open(os.path.join(pickle_folder, client, 'dictionary_df_' + client + '.pickle'), 'rb'))

    # TABLE DIMENSIONS
    joblist = [] # Create an empty joblist to fill it with all the jobs present in the client
    for key in dictionary:  # Iterate the dictionary key by key (file by file)
        job = str(dictionary[key]['JobName'][0]) # Extract the job name of the file
        # Append to list if not present
        if job not in joblist:
            joblist.append(job)
    # Create a counter table with as many rows as jobs present in the client and relevant counter variables to 0
    counter = pd.DataFrame(data={'JobName': joblist, 'Files': 0, 'Converter_Error': 0, 'EnergyAlarm_Low': 0,
                                 'EnergyAlarm_High': 0, 'C_Axis_Warning': 0})

    # TABLE FILLING
    for key in dictionary:  # Iterate the dictionary key by key (file by file)
        job = str(dictionary[key]['JobName'][0]) # Extract the job name of the file
        # Increment file counting in that job
        counter.loc[counter['JobName'] == job, ['Files']] = counter.loc[counter['JobName'] == job, ['Files']] + 1

        # If there is a value different from 0 in one of the error fields, increment the corresponding counter
        if not (dictionary[key]['Converter_Error'] == 0).all():
            counter.loc[counter['JobName'] == job, ['Converter_Error']] = counter.loc[counter['JobName'] == job,
                                                                                      ['Converter_Error']] + 1

        if not (dictionary[key]['EnergyAlarm_Low'] == 0).all():
            counter.loc[counter['JobName'] == job, ['EnergyAlarm_Low']] = counter.loc[counter['JobName'] == job,
                                                                                      ['EnergyAlarm_Low']] + 1

        if not (dictionary[key]['EnergyAlarm_High'] == 0).all():
            counter.loc[counter['JobName'] == job, ['EnergyAlarm_High']] = counter.loc[counter['JobName'] == job,
                                                                                       ['EnergyAlarm_High']] + 1

        if not (dictionary[key]['C_Axis_Warning'] == 0).all():
            counter.loc[counter['JobName'] == job, ['C_Axis_Warning']] = counter.loc[counter['JobName'] == job,
                                                                                     ['C_Axis_Warning']] + 1

    # FIGURE CREATION
    # Figure Size According to Client (due to different number of jobs)
    if client == 'Client1':
        figureSize = (10, 5)
    if client == 'Client2':
        figureSize = (10, 8)
    if client == 'Client3':
        figureSize = (10, 11)

    fig = plt.figure(figsize=figureSize) # Create figure with corresponding size
    ax = fig.add_subplot(1, 1, 1)  # Create subplot within figure for table insertion
    ax.axis('off')  # Turn off Axis
    ax.table(cellText=counter.values, colLabels=counter.columns, loc='center', cellLoc='center') # Add table
    ax.set_title('Error Counting ' + client)  # Add title
    plt.savefig(os.path.join(saving_path, 'Error Counting ' + client + '.png'), dpi=400)  # Save to image
