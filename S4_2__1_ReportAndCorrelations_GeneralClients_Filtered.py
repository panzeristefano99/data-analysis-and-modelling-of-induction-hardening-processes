"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.9
Description: ydata-profiling report and linear correlations over Client data (filtered)
"""

from ydata_profiling import ProfileReport
import pickle
import os
import numpy as np

# Folder with Client Data filtered in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')

# Creating directories to save Report data
report_folder = os.path.join(os.getcwd(), 'EDA_Files/ReportData_Correlations_GeneralClients_Filtered')
os.makedirs(os.path.join(report_folder), exist_ok=True)

clients = ['Client1', 'Client2', 'Client3']  # Client names
modes = ['ALL', 'ON', 'OFF']  # Operational modes

# Create subdirectories for each one of the clients
for client in clients:
    os.makedirs(os.path.join(report_folder, client), exist_ok=True)

# Dataset info for the generated report
dataset_info = {
        "author": "Stefano Fernando Panzeri Reyes",
        "copyright_holder": "ENRX AS",
        "copyright_year": 2023
    }

# Loop for data import and creation of Data Report for each general client Dataframe (in each one of the modes)
for client in clients:
    for mode in modes:
        for path, subdirs, files in os.walk(os.path.join(pickle_folder, client, mode)): # Analysing every client and mode folder
            for name in files:
                # Generate report only over General Clients Dataframe (not per job)
                if name.endswith('.pickle') and not name.startswith('dictionary') and len(name)<=21:
                    df = pickle.load(open(os.path.join(path, name), 'rb')) # Import of general Dataframe (pickle format)
                    profile = ProfileReport(df, dataset=dataset_info, title="Exploratory Analysis: " + name[3:-7],
                                            duplicates=None) # Report generation with no calculation of duplicates
                    profile.to_file(os.path.join(report_folder, client, "Report_" + name[3:-7] + ".html")) # Save of Report

                    # LINEAR CORRELATIONS CALCULATION
                    correlations = profile.description_set.correlations['auto']  # Get correlations from generated report
                    cm_reduced = correlations.where(np.triu(np.ones(correlations.shape)).astype(
                        bool))  # Reduce to the upper triangular (it is symmetric)
                    # Linearize Matrix to "table" (Variables and correlation coefficient)
                    cm_linear_reduced = cm_reduced.stack().reset_index()
                    cm_linear_reduced.columns = ['Variable 1', 'Variable 2', 'Correlation']  # Set Column names
                    # Remove correlation of variable with itself (1)
                    cm_linear_reduced = cm_linear_reduced.loc[cm_linear_reduced['Correlation'] < 1]
                    # Sort by descending correlation value and reset indexes
                    cm_linear_reduced = cm_linear_reduced.sort_values('Correlation', ascending=False).reset_index()
                    # Save all the correlations to a CSV file
                    cm_linear_reduced.to_csv(os.path.join(report_folder, client, 'Correlations_' + name[3:-7] + '.csv'))

                    # Filter by Strong correlations (over 0.6)
                    # Select only positive and negative correlations over 0.6 (not including 1, which is for each variable with itself)
                    strong_correlations = (cm_linear_reduced.loc[(cm_linear_reduced['Correlation'] < -0.6) | (
                                (cm_linear_reduced['Correlation'] > 0.6) & (cm_linear_reduced['Correlation'] < 1))])
                    # Sort by descending correlation value and reset indexes
                    strong_correlations=strong_correlations.sort_values('Correlation', ascending=False).reset_index()
                    # Save Strong correlations to a CSV file
                    strong_correlations.to_csv(os.path.join(report_folder, client, 'StrongCorrelations_'+name[3:-7]+'.csv'))
