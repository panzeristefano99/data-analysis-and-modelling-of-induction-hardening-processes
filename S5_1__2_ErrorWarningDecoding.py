def error_decoding(error_value, origin = "FC", error_warning="error"):
    """
    Author: Stefano Fernando Panzeri Reyes
    Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
    Collaborators: USN and ENRX
    Year: 2023
    License: MIT License (see the LICENSE file for the full license text)
    Python Version: 3.10
    :param error_value: Integer containing failure coming from PLC or Frequency Converter
    :param origin: String containing origin of the failure message: FC (Frequency Converter) or PLC
    :param error_warning: String containing type of failure: error or warning
    :return: List containing the Decoded Failure Message with Strings of the error/warnings present
    """
    # Sorted error/warning list according to the bit position in the received error message
    PLC_error_list = ["Adaption Error", "Synchronous Error", "Peripheral Error", "Positioning Error",
                      "Following Error Fault", "Homing Error", "Hardware Limit", "Software Limit",
                      "Communication Fault", "Dynamic Error", "Sensor Fault", "Drive Fault", "Command Not Accepted",
                      "User Fault", "Configuration Fault", "System Fault"]

    PLC_warning_list = ["Adaption Warning", "Synchronous Warning", "Peripheral Warning", "Positioning Warning",
                        "Following Error Warning", "Homing Warning", "Software Limit Max", "Software Limit Min",
                        "Communication Warning", "Dynamic Error", "Sensor Warning", "Drive Warning",
                        "Command Not Accepted", "User Warning", "Configuration Warning", "System Warning"]

    FC_error_list = ["Switch Mode Power Supply", "DC Overcurrent", "Main Fuse Alarm", "Fuse Fan", "Water flow low 3",
                     "Water Flow Low 2", "Water Flow Low 1", "Emergency Stop", "AC Overcurrent", "High Frequency",
                     "Low Frequency", "Inductive", "Overcurrent Driver", "Ground Fault", "DC Overvoltage",
                     "Power Supply", "Charge DC Voltage Failed", "Overtemperature Thermostat",
                     "System Reset by Watch Dog Timeout", "DC Voltage Too High", "DC Voltage Too Low",
                     "Power ON By Startup", "Capacitive", "External Fault", "Overtemperature Water Inlet",
                     "Ground Fault 2", "Water Flow Low 6", "Water Flow Low 5", "Water Flow Low 4", "LifeBit Timoeout",
                     "Inverter Common", "Inverter Overcurrent"]

    # Selecting right size and list to look into according to the input parameters
    if origin == "FC":
        length = "032b"  # 32 bits
        if error_warning == "error":
            searching_list = FC_error_list

    if origin == "PLC":
        length = "016b"  # 16 bits
        if error_warning == "error":
            searching_list = PLC_error_list
        elif error_warning == "warning":
            searching_list = PLC_warning_list

    # Formatting the obtained error value in a list of bits(binary values)
    error_value_bits = format(error_value, length)

    errorList=[]  # Empty output error list
    # Iterate the bits present in the received error message
    for i in range(len(error_value_bits)):
        # If the actual bit is 1, associate the text in the corresponding position of the decoding list
        if int(error_value_bits[i]):
            errorList.append(searching_list[i])

    #Check if the output error list is empty and return "NO ERROR" if that is the case
    if len(errorList) == 0:
        errorList = ["NO ERROR"]

    return errorList  # Return the ist with the decoded error/warning strings
