"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.10
Description: Filtration of collected data per client.
             Saving to Pandas DF per job (in each client) and per mode (ALL, ON, OFF).
             Saving also dictionary per client (with singular workpieces filtered)
"""

import os
import pandas as pd
import pickle

# Path with all the CSV files from the three clients from April to July
DATA_PATH = os.path.join(os.getcwd(),'DATA/ClientData')

# Variables to extract according to the client
variablesToUse_Client1 = ["Counter", "Automatic", "JobName", "C_AxisMode", "MachineIsEmpty", "HeatingOn",
                           "Converter_Setpoint", "Converter_Power", "Converter_DC_Voltage", "Converter_AC_Current",
                           "Converter_Frequency", "Converter_WaterFlow1", "Converter_WaterFlow2", "Y_Axis_Acceleration",
                           "Z_Axis_Acceleration", "C_Axis_Acceleration", "Y_Axis_Velocity", "Y_Axis_Position",
                           "Z_Axis_Velocity", "Z_Axis_Position", "C_Axis_Velocity", "C_Axis_Position",
                           "Inductor_WaterFlow", "Inductor_WaterTemp", "BusBar_WaterFlow", "BusBar_WaterTemp",
                           "Quench1_WaterFlow", "Quench1_WaterTemp", "Quench2_WaterFlow", "Quench2_WaterTemp",
                           "EnergyAlarm_Low", "EnergyAlarm_High", "Converter_Error", "C_Axis_Warning"]

variablesToUse_Client2 = ["Counter", "Automatic", "JobName", "C_AxisMode", "MachineIsEmpty",
                                  "HeatingOn", "Converter_Setpoint", "Converter_Power", "Converter_DC_Voltage",
                                  "Converter_AC_Current", "Converter_Frequency", "Y_Axis_Acceleration",
                                  "Z_Axis_Acceleration", "C_Axis_Acceleration", "Y_Axis_Velocity", "Y_Axis_Position",
                                  "Z_Axis_Velocity", "Z_Axis_Position", "C_Axis_Velocity", "C_Axis_Position",
                                  "Inductor_WaterFlow", "Inductor_WaterTemp", "BusBar_WaterFlow", "BusBar_WaterTemp",
                                  "EnergyAlarm_Low", "EnergyAlarm_High", "Converter_Error", "C_Axis_Warning"]

variablesToUse_Client3 = ["Counter", "Automatic", "JobName", "C_AxisMode", "MachineIsEmpty", "HeatingOn",
                       "Converter_Setpoint", "Converter_Power", "Converter_DC_Voltage", "Converter_AC_Current",
                       "Converter_Frequency", "Y_Axis_Acceleration", "Z_Axis_Acceleration", "C_Axis_Acceleration",
                       "Y_Axis_Velocity", "Y_Axis_Position", "Z_Axis_Velocity", "Z_Axis_Position", "C_Axis_Velocity",
                       "C_Axis_Position", "Inductor_WaterFlow", "Inductor_WaterTemp", "BusBar_WaterFlow",
                       "BusBar_WaterTemp", "Quench1_WaterFlow", "Quench1_WaterTemp", "EnergyAlarm_Low",
                       "EnergyAlarm_High", "Converter_Error", "C_Axis_Warning"]

# Function to load csv
def load_file(path_csv, clientToRead):
    variablesToUse = globals()['variablesToUse_' + clientToRead]
    return pd.read_csv(path_csv, usecols=variablesToUse)

# Creating general directory to save the filtered data
pickle_data_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')
os.makedirs(os.path.join(pickle_data_folder), exist_ok=True)

clients = ['Client1','Client2','Client3']  # Client names
modes = ['ALL', 'ON', 'OFF']  # Operational modes

# Create subdirectories for each one of the clients and machine modes
for client in clients:
    for mode in modes:
        os.makedirs(os.path.join(pickle_data_folder, client, mode), exist_ok=True)

# Creating relevant variables to store information
df_Client1 = pd.DataFrame()  # General dataframe with all the dataset of the client
dictionary_df_Client1 = {}  # Dictionary containing all the single dataframes (each csv) of the client
jobsClient1 = []  # List containing the jobs for each client

df_Client2 = pd.DataFrame()
dictionary_df_Client2 = {}
jobsClient2 = []

df_Client3 = pd.DataFrame()
dictionary_df_Client3 = {}
jobsClient3 = []

# Filling the lists and creating Pandas Dataframe values for each imported file
for client in clients:
    for path, subdirs, files in os.walk(os.path.join(DATA_PATH, client)):  # Analysing every client folder
        for name in files:
            if name.endswith('.csv') and 'ERROR' not in name:  # Check that is a .csv and does not contain any error
                df_read = load_file(os.path.join(path, name), client)  # Import csv into a Pandas DF

                # DATA FILTRATION AND ADJUSTMENT

                # Do not load csv that are run in manual mode or with the machine empty
                if not ((False in df_read['Automatic'].values) or (True in df_read['MachineIsEmpty'].values)):
                    # Remove those variable (now are constant)
                    df_read = df_read.drop(columns=["Automatic", "MachineIsEmpty"])
                    df_read = df_read.dropna()  # Remove empty entries (for last lines of csv)
                    # Set the Frequency, Power and Set-point to 0 when Current or Power are 0
                    df_read.loc[df_read['Converter_AC_Current'] == 0, ['Converter_Frequency', 'Converter_Power',
                                                                       'Converter_Setpoint']] = 0
                    df_read.loc[df_read['Converter_Power'] == 0, ['Converter_Frequency', 'Converter_AC_Current',
                                                                  'Converter_Setpoint']] = 0
                    # Calculating the Accumulated  Heating Cycle time and add it to the Dataset in the 4th column
                    df_read.insert(4, "HeatingCycleTime", 0)
                    for i in range(1, len(df_read)):
                        if df_read['Converter_Power'][i] != 0:
                            df_read.loc[[i], 'HeatingCycleTime'] = df_read['HeatingCycleTime'][i - 1] + 0.2
                        else:  # If the power is off then use the previous value of the column
                            df_read.loc[[i], 'HeatingCycleTime'] = df_read['HeatingCycleTime'][i - 1]
                    # Get the job name for classification of the dataframe
                    job = str(name[25:-4])
                    # Create a new df and add that job to the list if it is the first time that it appears
                    if job not in globals()['jobs' + client]:
                        globals()['jobs' + client].append(job)
                        globals()['df_' + client + '_' + job] = pd.DataFrame()

                    # Assure that the datatypes which are not numbers are correctly detected within the single df
                    df_read['JobName'] = df_read['JobName'].astype('category')
                    df_read['C_AxisMode'] = df_read['C_AxisMode'].astype('bool')
                    df_read['HeatingOn'] = df_read['HeatingOn'].astype('bool')
                    df_read['EnergyAlarm_Low'] = df_read['EnergyAlarm_Low'].astype('bool')
                    df_read['EnergyAlarm_High'] = df_read['EnergyAlarm_High'].astype('bool')

                    # Add the df to the global dictionary, to the df per job and to the client df
                    globals()['dictionary_df_' + client][name[:-4]] = df_read
                    globals()['df_' + client + '_' + job] = pd.concat([globals()['df_' + client + '_' + job], df_read])
                    globals()['df_' + client] = pd.concat([globals()['df_' + client], df_read])

    # Save all the dataframes and the dictionary in a .pickle format in the corresponding location

    for job in globals()['jobs' + client]:
        # ALL - Jobs
        # Reset index of Job ALL Dataframe
        globals()['df_' + client + '_' + job] = globals()['df_' + client + '_' + job].reset_index(drop=True)
        # Assure that the datatypes which are not numbers are correctly detected within the general df
        globals()['df_' + client + '_' + job]['JobName'] = globals()['df_' + client + '_' + job]['JobName'].astype(
            'category')
        globals()['df_' + client + '_' + job]['C_AxisMode'] = globals()['df_' + client + '_' + job][
            'C_AxisMode'].astype('bool')
        globals()['df_' + client + '_' + job]['HeatingOn'] = globals()['df_' + client + '_' + job]['HeatingOn'].astype(
            'bool')
        globals()['df_' + client + '_' + job]['EnergyAlarm_Low'] = globals()['df_' + client + '_' + job][
            'EnergyAlarm_Low'].astype('bool')
        globals()['df_' + client + '_' + job]['EnergyAlarm_High'] = globals()['df_' + client + '_' + job][
            'EnergyAlarm_High'].astype('bool')
        # Save Job ALL Dataframe
        with open(os.path.join(pickle_data_folder, client, 'ALL', 'df_' + client + '_' + job + '.pickle'), 'wb') as f:
            pickle.dump(globals()['df_' + client + '_' + job], f)

        # ON - Jobs
        # Create Job ON Dataframe and reset index (get only when converter is ON (power is not 0)
        globals()['df_' + client + '_' + job + '_ON'] = globals()['df_' + client + '_' + job][
            globals()['df_' + client + '_' + job]['Converter_Power'] != 0].reset_index(drop=True)
        # Assure that the datatypes which are not numbers are correctly detected within the general df
        globals()['df_' + client + '_' + job + '_ON']['JobName'] = globals()['df_' + client + '_' + job + '_ON'][
            'JobName'].astype('category')
        globals()['df_' + client + '_' + job + '_ON']['C_AxisMode'] = globals()['df_' + client + '_' + job + '_ON'][
            'C_AxisMode'].astype('bool')
        globals()['df_' + client + '_' + job + '_ON']['HeatingOn'] = globals()['df_' + client + '_' + job + '_ON'][
            'HeatingOn'].astype('bool')
        globals()['df_' + client + '_' + job + '_ON']['EnergyAlarm_Low'] = \
            globals()['df_' + client + '_' + job + '_ON']['EnergyAlarm_Low'].astype('bool')
        globals()['df_' + client + '_' + job + '_ON']['EnergyAlarm_High'] = \
            globals()['df_' + client + '_' + job + '_ON']['EnergyAlarm_High'].astype('bool')
        # Save Job ON Dataframe to pickle
        with open(os.path.join(pickle_data_folder, client, 'ON', 'df_' + client + '_' + job + '_ON' + '.pickle'),
                  'wb') as f:
            pickle.dump(globals()['df_' + client + '_' + job + '_ON'], f)

        # OFF - Jobs
        # Create Job OFF Dataframe and reset index (get only when converter is OFF (power is 0)
        globals()['df_' + client + '_' + job + '_OFF'] = globals()['df_' + client + '_' + job][
            globals()['df_' + client + '_' + job]['Converter_Power'] == 0].reset_index(drop=True)
        # Assure that the datatypes which are not numbers are correctly detected within the general df
        globals()['df_' + client + '_' + job + '_OFF']['JobName'] = globals()['df_' + client + '_' + job + '_OFF'][
            'JobName'].astype('category')
        globals()['df_' + client + '_' + job + '_OFF']['C_AxisMode'] = globals()['df_' + client + '_' + job + '_OFF'][
            'C_AxisMode'].astype('bool')
        globals()['df_' + client + '_' + job + '_OFF']['HeatingOn'] = globals()['df_' + client + '_' + job + '_OFF'][
            'HeatingOn'].astype('bool')
        globals()['df_' + client + '_' + job + '_OFF']['EnergyAlarm_Low'] = \
            globals()['df_' + client + '_' + job + '_OFF']['EnergyAlarm_Low'].astype('bool')
        globals()['df_' + client + '_' + job + '_OFF']['EnergyAlarm_High'] = \
            globals()['df_' + client + '_' + job + '_OFF']['EnergyAlarm_High'].astype('bool')
        # Save JOb OFF Dataframe
        with open(os.path.join(pickle_data_folder, client, 'OFF', 'df_' + client + '_' + job + '_OFF' + '.pickle'),
                  'wb') as f:
            pickle.dump(globals()['df_' + client + '_' + job + '_OFF'], f)

    # ALL
    # Reset index of General ALL dataframe
    globals()['df_' + client] = globals()['df_' + client].reset_index(drop=True)
    # Assure that the datatypes which are not numbers are correctly detected within the general df
    globals()['df_' + client]['JobName'] = globals()['df_' + client][
        'JobName'].astype(
        'category')
    globals()['df_' + client]['C_AxisMode'] = globals()['df_' + client]['C_AxisMode'].astype('bool')
    globals()['df_' + client]['HeatingOn'] = globals()['df_' + client]['HeatingOn'].astype('bool')
    globals()['df_' + client]['EnergyAlarm_Low'] = globals()['df_' + client]['EnergyAlarm_Low'].astype('bool')
    globals()['df_' + client]['EnergyAlarm_High'] = globals()['df_' + client]['EnergyAlarm_High'].astype('bool')
    # Save General ALL dataframe
    with open(os.path.join(pickle_data_folder, client, 'ALL', 'df_' + client + '.pickle'), 'wb') as f:
        pickle.dump(globals()['df_' + client], f)

    # ON
    # Create General ON Dataframe and reset index (get only when converter is ON (power is not 0)
    globals()['df_' + client + '_ON'] = globals()['df_' + client][
        globals()['df_' + client]['Converter_Power'] != 0].reset_index(drop=True)
    # Assure that the datatypes which are not numbers are correctly detected  within the general df
    globals()['df_' + client + '_ON']['JobName'] = globals()['df_' + client + '_ON']['JobName'].astype('category')
    globals()['df_' + client + '_ON']['C_AxisMode'] = globals()['df_' + client + '_ON']['C_AxisMode'].astype('bool')
    globals()['df_' + client + '_ON']['HeatingOn'] = globals()['df_' + client + '_ON']['HeatingOn'].astype('bool')
    globals()['df_' + client + '_ON']['EnergyAlarm_Low'] = \
        globals()['df_' + client + '_ON']['EnergyAlarm_Low'].astype('bool')
    globals()['df_' + client + '_ON']['EnergyAlarm_High'] = \
        globals()['df_' + client + '_ON']['EnergyAlarm_High'].astype('bool')
    # Save General ON Dataframe
    with open(os.path.join(pickle_data_folder, client, 'ON', 'df_' + client + '_ON' + '.pickle'), 'wb') as f:
        pickle.dump(globals()['df_' + client + '_ON'], f)

    # OFF
    # Create General OFF Dataframe and reset index (get only when converter is OFF (power is 0)
    globals()['df_' + client + '_OFF'] = globals()['df_' + client][
        globals()['df_' + client]['Converter_Power'] == 0].reset_index(drop=True)
    # Assure that the datatypes which are not numbers are correctly detected  within the general df
    globals()['df_' + client + '_OFF']['JobName'] = globals()['df_' + client + '_OFF']['JobName'].astype('category')
    globals()['df_' + client + '_OFF']['C_AxisMode'] = globals()['df_' + client + '_OFF']['C_AxisMode'].astype('bool')
    globals()['df_' + client + '_OFF']['HeatingOn'] = globals()['df_' + client + '_OFF']['HeatingOn'].astype('bool')
    globals()['df_' + client + '_OFF']['EnergyAlarm_Low'] = \
        globals()['df_' + client + '_OFF']['EnergyAlarm_Low'].astype('bool')
    globals()['df_' + client + '_OFF']['EnergyAlarm_High'] = \
        globals()['df_' + client + '_OFF']['EnergyAlarm_High'].astype('bool')
    # Save General OFF Dataframe
    with open(os.path.join(pickle_data_folder, client, 'OFF', 'df_' + client + '_OFF' + '.pickle'), 'wb') as f:
        pickle.dump(globals()['df_' + client + '_OFF'], f)

    # Save dictionary with individual file values for each Client
    with open(os.path.join(pickle_data_folder, client, 'dictionary_df_' + client + '.pickle'), 'wb') as f:
        pickle.dump(globals()['dictionary_df_' + client], f)
