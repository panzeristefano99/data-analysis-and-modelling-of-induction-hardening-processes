"""
Author: Stefano Fernando Panzeri Reyes
Master Thesis: Data Analysis and Modelling of Induction Hardening Processes
Collaborators: USN and ENRX
Year: 2023
License: MIT License (see the LICENSE file for the full license text)
Python Version: 3.9
Description: Generation of Scatter Matrix over Client Data (filtered)
"""

import matplotlib.pyplot as plt
import pickle
import os
import seaborn as sns
import gc

# Folder with Client Data filtered in pickle format
pickle_folder = os.path.join(os.getcwd(), 'DATA/PickleData_Filtered')

# Creating directories to save Report data
scatter_folder = os.path.join(os.getcwd(), 'EDA_Files/ScatterMatrix_Clients')
os.makedirs(os.path.join(scatter_folder), exist_ok=True)

clients = ['Client1', 'Client2', 'Client3']  # Client names
modes = ['ALL', 'ON', 'OFF']  # Operational modes

# Create subdirectories for each one of the clients
for client in clients:
    os.makedirs(os.path.join(scatter_folder, client), exist_ok=True)

# Loop for data import and creation of Scatter Matrix for each general client Dataframe (in each one of the modes)
for client in clients:
    for mode in modes:
        for path, subdirs, files in os.walk(os.path.join(pickle_folder, client, mode)): # Analysing every client and mode folder
            for name in files:
                # Generate Scatter Matrix only over General Clients Dataframe (not per job)
                if name.endswith('.pickle') and not name.startswith('dictionary') and len(name)<=21:

                    df = pickle.load(open(os.path.join(path, name), 'rb'))  # Import of general Dataframe (pickle format)
                    pp = sns.pairplot(df, corner=True)  # creation of Scatter Matrix Plot
                    pp.fig.suptitle('ScatterMatrix_' + name[3:-7], fontsize='xx-large') # Add title
                    plt.savefig(os.path.join(scatter_folder, client, 'ScatterMatrix_' + name[3:-7] + '.png')) # Save figure

                    # Delete variables, clean and close plot and collect "garbage" for cleaning use of memory (RAM demanding)
                    plt.clf()
                    del pp
                    gc.collect()
                    plt.close()
